release:
	g++ main.cpp -std=c++11 -O3 -s

debug:
	g++ main.cpp -std=c++11 -g -Wextra -Wall
