#include<bits/stdc++.h>
#define SEED 0
#define SMART_KS
//#define ENLARGE
using namespace std;

//git push test
/** Heuristic variables **/

const int TIME = 10, TASK = 80, SERVER = 3, TYPES = 3;
const int BASE_CPU = 100, BASE_MEMORY = 100, MAX_V = 100, BASE_FIX_COST = 80;
const double RATE = 0.5, COST_RATE = 1.5; // 等級提升一級所產生的倍率
const int MAX_CPU = ( 1 + RATE * ( TYPES - 1 ) ) * SERVER * BASE_CPU, MAX_MEMORY = ( 1 + RATE * ( TYPES - 1 ) ) * SERVER * BASE_MEMORY;
int LV[ SERVER ], CPU[ SERVER ], MEMORY[ SERVER ], FIX_COST[ SERVER ], REOPEN_COST[ SERVER ], TOTAL_CPU, TOTAL_MEMORY;	// 稍候再算,只好先捨棄 const

int arrive[ TASK + 1 ], delta[ TIME ][ TASK + 1 ], value[ TIME ][ TASK + 1 ], cpu[ TIME ][ TASK + 1 ], memory[ TIME ][ TASK + 1 ];
int gain[ TASK + 1 ][ MAX_CPU + 1 ][ MAX_MEMORY + 1 ];
int opt_cpu[ TIME ], opt_mem[ TIME ];

double ha[ TIME ][ TASK ][ SERVER ], hx[ TIME ][ SERVER ], hy[ TIME ][ SERVER ], hb[ TASK ]; // heuristic solutions
#ifdef SMART_KS
vector<int> content[ TASK + 1 ][ MAX_CPU + 1 ][ MAX_MEMORY + 1 ];
#else
bool content[ TASK + 1 ][ MAX_CPU + 1 ][ MAX_MEMORY + 1 ];
#endif
vector<int> served( TASK + 1, -1 );

inline bool better( int i, int j, int k, int t )
{
	return value[ t ][ i ] + gain[ i - 1 ][ j - cpu[ t ][ i ] ][ k - memory[ t ][ i ] ] > gain[ i - 1 ][ j ][ k ];
}
void init();
void testing( const vector<int>&, int, int );
void next_fit( const vector<int>&, int, int ), best_fit( const vector<int>&, int, int ), first_fit( const vector<int>&, int, int ), knapsack( int ), bin_packing( int );

/** LR variables **/

double z_star = 0, zd = INT_MIN, mu1[ TIME ][ TASK ][ SERVER ], mu2[ TIME ][ SERVER ], mu3[ TIME ][ SERVER ], mu4[ TIME ][ SERVER ], mu5[ TASK ], mu6[ TASK ];
double iter, NumofImprovement, LB = INT_MIN, lambda = 2, ITER_LIMIT = 10000, IMPROVE_LIMIT = 500;
double block_rate = 0.001;

const int CP_SORT = 0, MU_SORT = 1;
const int heu3 = CP_SORT, heu4 = CP_SORT;

int GAMMA[ TASK ], penalty[ TASK ], epsilon = TIME / 2, OPEN = 1;
double a[ TIME ][ TASK ][ SERVER ], x[ TIME ][ SERVER ], y[ TIME ][ SERVER ], b[ TASK ];
double pa[ TIME ][ TASK ][ SERVER ], px[ TIME ][ SERVER ], py[ TIME ][ SERVER ], pb[ TASK ];
double pcpu[ TIME ][ TASK + 1 ], pmemory[ TIME ][ TASK + 1 ];

/** LR functions **/

void LR(), adjust_multiplier();
double sol_sub1(), sol_sub2(), sol_sub3(), sol_sub4(), cal_sub1(), cal_sub2(), cal_sub3(), cal_sub4();
double primal(), cal_primal( double use[ TIME ][ TASK + 1 ], int, int );

template<typename a, typename b>
void see( vector<pair<a,b>>& v )
{
	for( auto& i: v )
		cout << i.first << '\t' << i.second << endl;
}

template<typename T>
void see( vector<T>& v )
{
	for( auto i: v )
		cout << i << ' ';
	cout << endl;
}

void see_solution()
{
	puts( "pa:" );
	for( int t = 0; t < TIME; ++t )
	{
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				printf( "%.2f%c", pa[ t ][ i ][ s ], s == SERVER - 1? '\n' : ' ' );
		puts( "_" );
	}
	puts( "px: " );
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
				printf( "%.2f%c", px[ t ][ s ], s == SERVER - 1? '\n' : ' ' );
	puts( "py: " );
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
				printf( "%.2f%c", py[ t ][ s ], s == SERVER - 1? '\n' : ' ' );
}
/** Main process **/

int main()
{
	/** Heuristic method -- Knapsack & bin packing **/

	for( int t = 0; t < TIME; ++t )
	{
		knapsack( t );
		bin_packing( t );
	}
	if( find( served.begin(), served.end(), -1 ) != served.end() )
	{
		cout << "Unpacked items:";
		for( int i = 1; i <= TASK; ++i )
			if( served[ i ] == -1 )
			{
				cout << ' ' << i;
				hb[ i ] = 1;
			}
		cout << endl;
		cout << "Dropping rate = " << count( served.begin() + 1, served.end(), -1 ) / double( TASK ) << endl;
	}

	//Determine z_star

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			z_star += ( FIX_COST[ s ] * hx[ t ][ s ] ) + ( REOPEN_COST[ s ] * hy[ t ][ s ] );
			for( int i = 0; i < TASK; ++i )
			{
				z_star -= value[ t ][ i + 1 ] * delta[ t ][ i + 1 ] * ha[ t ][ i ][ s ];
				z_star += penalty[ i ] * hb[ i ];
			}
		}
	printf( "z_star = %f\n", z_star );

	/** LR method **/

	init();
	for( int t = 1; t < TIME; ++t )
	{
		memcpy( cpu[ t ], cpu[ t - 1 ], sizeof( cpu[ t - 1 ] ) );
		memcpy( memory[ t ], memory[ t - 1 ], sizeof( memory[ t - 1 ] ) );
		memcpy( value[ t ], value[ t - 1 ], sizeof( value[ t - 1 ] ) );
	}
	LR();
}

void init()
{
	srand( SEED );

	/** Initialize servers **/

	puts( "Server\tCPU\tMemory" );
	for( int s = 0; s < SERVER; ++s )
		LV[ s ] = rand() % TYPES;
//	sort( LV, LV + SERVER, greater<int>() ); // descending
	sort( LV, LV + SERVER ); // ascending
	for( int s = 0; s < SERVER; ++s )
	{
		CPU[ s ] = ( 1 + LV[ s ] * RATE ) * BASE_CPU;
		MEMORY[ s ] = ( 1 + LV[ s ] * RATE ) * BASE_MEMORY;
		FIX_COST[ s ] = ( 1 + LV[ s ] * RATE ) * BASE_FIX_COST;
		REOPEN_COST[ s ] = FIX_COST[ s ] * COST_RATE;
		printf( "%d\t%d\t%d\n", s + 1, LV[ s ], LV[ s ] );
	}
	puts( "" );

	/** Initialize tasks **/

	puts( "Task\tValue\tCPU\tMemory\tArrive" );
	for( int i = 1, come; i <= TASK; ++i )
	{
		value[ 0 ][ i ] = rand() % MAX_V + 1;
		pcpu[ 0 ][ i ] = cpu[ 0 ][ i ] = rand() % BASE_CPU / 2 + 1;
		pmemory[ 0 ][ i ] = memory[ 0 ][ i ] = rand() % BASE_MEMORY / 2 + 1;
		//come = rand() % TIME;
		come = 0;
		arrive[ i ] = come;
		for( int start = come; start < TIME; ++start )
		//for( int start = 0; start < TIME; ++start )
			delta[ start ][ i ] = 1;
		printf( "%d\t%d\t%d\t%d\t%d\n", i, value[ 0 ][ i ], cpu[ 0 ][ i ], memory[ 0 ][ i ], come );
	}
	for( int i = 1; i < TIME; ++i )
	{
		memcpy( pcpu[ i ], pcpu, sizeof( pcpu[ 0 ] ) );
		memcpy( pmemory[ i ], pmemory, sizeof( pmemory[ 0 ] ) );
	}
	puts( "" );

	TOTAL_CPU = accumulate( CPU, CPU + SERVER, 0 );
	TOTAL_MEMORY = accumulate( MEMORY, MEMORY + SERVER, 0 );

	/** Initialize LR variables **/

	for( int& i: GAMMA )
		i = 1;
	for( int& i: penalty )
		i = 200;
	memset( mu1, 0, sizeof( mu1 ) );
	memset( mu2, 0, sizeof( mu2 ) );
	memset( mu3, 0, sizeof( mu3 ) );
	memset( mu4, 0, sizeof( mu4 ) );
}

void knapsack( int t )
{
	if( t == 0 )
		init();
	else
	{
		//clear demands of task if it is served.
		memcpy( cpu[ t ], cpu[ t - 1 ], sizeof( cpu[ t - 1 ] ) );
		memcpy( memory[ t ], memory[ t - 1 ], sizeof( memory[ t - 1 ] ) );
		memcpy( value[ t ], value[ t - 1 ], sizeof( value[ t - 1 ] ) );
		for( int i = 1; i <= TASK; ++i )
			if( served[ i ] != -1 )
				cpu[ t ][ i ] = memory[ t ][ i ] = 0;
	}
	// knapsack recurrence relationship
	for( int i = 1; i <= TASK; ++i )
		for( int j = 1; j <= TOTAL_CPU; ++j )
			for( int k = 1; k <= TOTAL_MEMORY; ++k )
				if( delta[ t ][ i ] == 1 && served[ i ] == -1 && cpu[ t ][ i ] && memory[ t ][ i ] && cpu[ t ][ i ] <= j && memory[ t ][ i ] <= k && better( i, j, k, t ) )
				{
					gain[ i ][ j ][ k ] = value[ t ][ i ] + gain[ i - 1 ][ j - cpu[ t ][ i ] ][ k - memory[ t ][ i ] ];
#ifdef SMART_KS
					content[ i ][ j ][ k ] = content[ i - 1 ][ j - cpu[ t ][ i ] ][ k - memory[ t ][ i ] ];
					content[ i ][ j ][ k ].push_back( i );
#else
					content[ i ][ j ][ k ] = true;
#endif
				}
				else
				{
					gain[ i ][ j ][ k ] = gain[ i - 1 ][ j ][ k ];
#ifdef SMART_KS
					content[ i ][ j ][ k ] = content[ i - 1 ][ j ][ k ];
#else
					content = false;
#endif
				}

	int sum_cpu = 0, sum_memory = 0;
	vector<int>& result = content[ TASK ][ TOTAL_CPU ][ TOTAL_MEMORY ];

	printf( "\n\nMaximum possible value = %d\nThe pack contains:", gain[ TASK ][ TOTAL_CPU ][ TOTAL_MEMORY ] );
	for( auto& c: result )
		printf( " %d", c ), sum_cpu += cpu[ t ][ c ], sum_memory += memory[ t ][ c ];
	printf( "\nTotal CPU = %d\nTotal memory = %d\n", opt_cpu[ t ] = sum_cpu, opt_mem[ t ] = sum_memory );
}

void bin_packing( int t )
{
	vector<int>& result = content[ TASK ][ TOTAL_CPU ][ TOTAL_MEMORY ];
	puts( "\n\n	***********\n	Order by ID\n	***********\n" );
	testing( result, SERVER, t );

//	puts( "\n\n	**********************\n	Order by CPU ascending\n	**********************\n" );
//	sort( result.begin(), result.end(), [ t ] ( int i, int j ) { return cpu[ t ][ i ] < cpu[ t ][ j ]; } );
//	testing( result, SERVER, t );

//	puts( "\n\n	***********************\n	Order by CPU descending\n	***********************\n" );
//	sort( result.begin(), result.end(), [ t ] ( int i, int j ) { return cpu[ t ][ i ] > cpu[ t ][ j ]; } );
//	testing( result, SERVER, t );

//	puts( "\n\n	*************************\n	Order by memory ascending\n	*************************\n" );
//	sort( result.begin(), result.end(), [ t ] ( int i, int j ) { return memory[ t ][ i ] < memory[ t ][ j ]; } );
//	testing( result, SERVER, t );
////
//	puts( "\n\n	**************************\n	Order by memory descending\n	**************************\n" );
//	sort( result.begin(), result.end(), [ t ] ( int i, int j ) { return memory[ t ][ i ] > memory[ t ][ j ]; } );
//	testing( result, SERVER, t );

}

void testing( const vector<int>& result, int SERVER, int t )
{
	// Use algorithms for z_star
	puts( "\nNext-fit algorithm\n" );
	next_fit( result, SERVER, t );

//	puts( "\nBest-fit algorithm\n" );
//	best_fit( result, SERVER, t );
//
//	puts( "\nFirst-fit algorithm\n" );
//	first_fit( result, SERVER, t );

}

#ifdef ENLARGE
// CPU 的部份,這裡就不加上 [S] 了,將來有一天要開這裡的話要記得改它們
void next_fit( const vector<int>& result, int bin_num, int t )
{
	bool found = false;
	for( int local_cpu = CPU, local_memory = MEMORY; ; local_cpu += !found, local_memory += !found )
	{
		auto it = result.begin();
		int C = 0, M = 0, TV = 0;

		for( int i = 0, tc = 0, tm = 0, tv = 0; i < bin_num && it != result.end(); ++i, tc = tm = tv = 0 )
		{
			if( found )
				printf( "Server %d contains:", i );
			for( int c = 0, m = 0; it != result.end() && c + cpu[ t ][ *it ] <= local_cpu && m + memory[ t ][ *it ] <= local_memory; ++it )
			{
				if( found )
					printf( " %d", *it );
				c += cpu[ t ][ *it ];
				m += memory[ t ][ *it ];
				C += cpu[ t ][ *it ];
				M += memory[ t ][ *it ];
				tc += cpu[ t ][ *it ];
				tm += memory[ t ][ *it ];
				tv += value[ t ][ *it ];
				TV += value[ t ][ *it ];
			}
			if( found )
				printf( "\tCPU = %d\tMemory = %d\tValue = %d\n", tm, tc, tv );
		}
		if( found )
		{
			printf( "max CPU    = %d\tcurrent CPU    = %d\nmax memory = %d\tcurrent memory = %d\n", opt_cpu, C, opt_mem, M );
			printf( "Current local CPU = %d\tlocal memory = %d\tvalue = %d\n", local_cpu, local_memory, TV - FIX_COST * SERVER );
			puts( it == result.end()? "All tasks are packed" : "Not all tasks are packed" );
			break;
		}
		found |= it == result.end();
	}
}

void best_fit( const vector<int>& result, int bin_num, int t )
{
	bool found = false;
	for( int local_cpu = CPU, local_memory = MEMORY; !found; local_cpu += !found, local_memory += !found )
	{
		vector<int> *bin = new vector<int>[ bin_num ];
		int *scpu = new int[ bin_num ], *smem = new int[ bin_num ], C = 0, M = 0, TV = 0;
		bool all = true;

		fill( scpu, scpu + bin_num, 0 );
		fill( smem, smem + bin_num, 0 );
		for( auto it = result.begin(); it != result.end(); ++it )
		{
			int ins = -1, cap = INT_MAX;

			for( int i = 0; i < bin_num; ++i )
			{
				int c = local_cpu - scpu[ i ], m = local_memory - smem[ i ];
				if( c >= cpu[ t ][ *it ] && m >= memory[ t ][ *it ] && min( c, m ) < cap )
				{
					cap = min( c, m );
					ins = i;
				}
			}

			if( ins == -1 )
				all = false;
			else
			{
				bin[ ins ].push_back( *it );
				scpu[ ins ] += cpu[ t ][ *it ];
				smem[ ins ] += memory[ t ][ *it ];
			}
		}

		if( !all )
		{
			delete[] scpu;
			delete[] smem;
			continue;
		}
		else
			found = true;

		for( int i = 0; i < bin_num; ++i )
		{
			int tv = 0;
			printf( "Server %d contains:", i );
			for( auto j: bin[ i ] )
				printf( " %d", j ), tv += value[ t ][ j ];
			printf( "\tCPU = %d\tMemory = %d\tValue = %d\n", scpu[ i ], smem[ i ], tv );
			C += scpu[ i ];
			M += smem[ i ];
			TV += tv;
		}
		printf( "max CPU    = %d\tcurrent CPU    = %d\nmax memory = %d\tcurrent memory = %d\n", opt_cpu, C, opt_mem, M );
		printf( "Current local CPU = %d\tlocal memory = %d\tvalue = %d\n", local_cpu, local_memory, TV - FIX_COST * SERVER );
		puts( all? "All tasks are packed" : "Not all tasks are packed" );
		delete[] scpu;
		delete[] smem;
	}
}

void first_fit( const vector<int>& result, int bin_num, int t )
{
	bool found = false;
	for( int local_cpu = CPU, local_memory = MEMORY; !found; local_cpu += !found, local_memory += !found )
	{
		vector<int> *bin = new vector<int>[ bin_num ];
		int *scpu = new int[ bin_num ], *smem = new int[ bin_num ], C = 0, M = 0, TV = 0;
		bool all = true;

		fill( scpu, scpu + bin_num, 0 );
		fill( smem, smem + bin_num, 0 );
		for( auto it = result.begin(); it != result.end(); ++it )
		{
			int ins = -1;

			for( int i = 0; i < bin_num && ins == -1; ++i )
				if( local_cpu - scpu[ i ] >= cpu[ t ][ *it ] && local_memory - smem[ i ] >= memory[ t ][ *it ] )
					ins = i;

			if( ins == -1 )
				all = false;
			else
			{
				bin[ ins ].push_back( *it );
				scpu[ ins ] += cpu[ t ][ *it ];
				smem[ ins ] += memory[ t ][ *it ];
			}
		}

		if( !all )
		{
			delete[] scpu;
			delete[] smem;
			continue;
		}
		else
			found = true;

		for( int i = 0; i < bin_num; ++i )
		{
			int tv = 0;
			printf( "Server %d contains:", i );
			for( auto j: bin[ i ] )
				printf( " %d", j ), tv += value[ t ][ j ];
			printf( "\tCPU = %d\tMemory = %d\tValue = %d\n", scpu[ i ], smem[ i ], tv );
			C += scpu[ i ];
			M += smem[ i ];
			TV += tv;
		}
		printf( "max CPU    = %d\tcurrent CPU    = %d\nmax memory = %d\tcurrent memory = %d\n", opt_cpu, C, opt_mem, M );
		printf( "Current local CPU = %d\tlocal memory = %d\tvalue = %d\n", local_cpu, local_memory, TV - FIX_COST * SERVER );
		puts( all? "All tasks are packed" : "Not all tasks are packed" );
		delete[] scpu;
		delete[] smem;
	}
}

#else

void next_fit( const vector<int>& result, int bin_num, int t )
{
	auto it = result.begin();
	int C = 0, M = 0, total_value = 0;

	for( int s = 0; s < bin_num && it != result.end(); ++s )
	{
		printf("Time is %d\n", t);
		printf( "Server %d contains:", s );
		for( int c = 0, m = 0; it != result.end() && c + cpu[ t ][ *it ] <= CPU[ s ] && m + memory[ t ][ *it ] <= MEMORY[ s ]; ++it )
		{
			printf( " %d", *it );
			c += cpu[ t ][ *it ];
			m += memory[ t ][ *it ];
			C += cpu[ t ][ *it ];
			M += memory[ t ][ *it ];
			total_value += value[ t ][ *it ];
			hx[ t ][ s ] = ha[ t ][ *it - 1 ][ s ] = 1;
			served[ *it ] = t;
		}
		puts( "" );

		//calculate ys
		if( hx[ t ][ s ] && ( !t || !hx[ t - 1 ][ s ] ) && any_of( hx, hx + t, [ s ] ( double *n ) { return n[ s ] == 1; } ) )
			hy[ t ][ s ] = 1;

	}
	printf( "max CPU    = %d\tcurrent CPU    = %d\nmax memory = %d\tcurrent memory = %d\n", opt_cpu[ t ], C, opt_mem[ t ], M );
	printf( it == result.end()? "All tasks are packed\n" : "Not all tasks are packed\n" );
}

void best_fit( const vector<int>& result, int bin_num, int t )
{
	vector<int> *bin = new vector<int>[ bin_num ];
	int *scpu = new int[ bin_num ], *smem = new int[ bin_num ], C = 0, M = 0; // scpu: cpu used
	bool all = true;

	fill( scpu, scpu + bin_num, 0 );
	fill( smem, smem + bin_num, 0 );
	auto it = result.begin();
	for( ; it != result.end(); ++it )
	{
		int ins = -1, cap = INT_MAX;

		for( int i = 0; i < bin_num; ++i )
		{
			int c = CPU[ i ] - scpu[ i ], m = MEMORY[ i ] - smem[ i ];
			if( c >= cpu[ t ][ *it ] && m >= memory[ t ][ *it ] && min( c, m ) < cap )
			{
				cap = min( c, m );
				ins = i;
			}
		}

		if( ins == -1 )
			all = false;
		else
		{
			bin[ ins ].push_back( *it );
			scpu[ ins ] += cpu[ t ][ *it ];
			smem[ ins ] += memory[ t ][ *it ];
			hx[ t ][ ins ] = ha[ t ][ *it - 1 ][ ins ] = 1;
			served[ *it ] = t;
		}
	}

	for( int s = 0; s < bin_num; ++s )
	{
		printf( "Time is %d\n", t );
		printf( "Server %d contains:", s );
		for( auto j: bin[ s ] )
			printf( " %d", j );
		puts( "" );
		C += scpu[ s ];
		M += smem[ s ];

		if( hx[ t ][ s ] && ( !t || !hx[ t - 1 ][ s ] ) && any_of( hx, hx + t, [ s ] ( double *n ) { return n[ s ] == 1; } ) )
			hy[ t ][ s ] = 1;
	}

	printf( "max CPU    = %d\tcurrent CPU    = %d\nmax memory = %d\tcurrent memory = %d\n", opt_cpu[ t ], C, opt_mem[ t ], M );
	puts( it == result.end()? "All tasks are packed" : "Not all tasks are packed" );
//	puts( all? "All tasks are packed" : "Not all tasks are packed" );
}

void first_fit( const vector<int>& result, int bin_num, int t )
{
	vector<int> *bin = new vector<int>[ bin_num ];
	int *scpu = new int[ bin_num ], *smem = new int[ bin_num ], C = 0, M = 0;
	bool all = true;

	fill( scpu, scpu + bin_num, 0 );
	fill( smem, smem + bin_num, 0 );
	auto it = result.begin();
	for( ; it != result.end(); ++it )
	{
		int ins = -1;

		for( int i = 0; i < bin_num && ins == -1; ++i )
			if( CPU[ i ] - scpu[ i ] >= cpu[ t ][ *it ] && MEMORY[ i ] - smem[ i ] >= memory[ t ][ *it ] )
				ins = i;

		if( ins == -1 )
			all = false;
		else
		{
			bin[ ins ].push_back( *it );
			scpu[ ins ] += cpu[ t ][ *it ];
			smem[ ins ] += memory[ t ][ *it ];
			hx[ t ][ ins ] = ha[ t ][ *it - 1 ][ ins ] = 1;
			served[ *it ] = t;
		}
	}

	for( int s = 0; s < bin_num; ++s )
	{
		printf("Time is %d\n", t);
		printf( "Server %d contains:", s );
		for( auto j: bin[ s ] )
			printf( " %d", j );
		puts( "" );
		C += scpu[ s ];
		M += smem[ s ];


		//calculate ys
		if( hx[ t ][ s ] && ( !t || !hx[ t - 1 ][ s ] ) && any_of( hx, hx + t, [ s ] ( double *n ) { return n[ s ] == 1; } ) )
			hy[ t ][ s ] = 1;
	}
	printf( "max CPU    = %d\tcurrent CPU    = %d\nmax memory = %d\tcurrent memory = %d\n", opt_cpu[ t ], C, opt_mem[ t ], M );
	puts( all? "All tasks are packed" : "Not all tasks are packed" );
}

#endif

void LR()
{
	for( int iter = 0; iter < ITER_LIMIT; ++iter )
	{
		/* 1. Solve sub-problems
		 * 2. Get decision variables
		 * 3. Tune them if not feasible
		 * 4. Cal optimal value
		 */
//		cout << "#\n" << sol_sub1() << endl << sol_sub2() << endl << sol_sub3() << endl << sol_sub4() << endl;
//		return;
		zd = sol_sub1() + sol_sub2() + sol_sub3() + sol_sub4();
		double UB = primal();
		// Update primal feasible solution
		//if( z_star > UB )
			//see_solution();
		z_star = min( z_star, UB );
		LB = max( LB, zd );

		// Update lower bound
//		if( LB >= zd )
//			++NumofImprovement;
//		else
//			LB = zd, NumofImprovement = 0;
// chris change

		if( zd > 1e10 )
			cout << sol_sub1() << ' ' << sol_sub2() << ' ' << sol_sub3() << ' ' << sol_sub4() << endl;

		if( LB < zd )
		{
			double max_improve = NumofImprovement;
			LB = zd, NumofImprovement = 0;
//			cout << "max improve = " << max_improve << '\t'<< endl;
		}
		else
			++NumofImprovement;

//		cout << "Num of Improvement = " << NumofImprovement << '\t' << "lambda = " << lambda << '\t' <<"upper bound = " << z_star << '\t' << "lower bound = " << LB << endl;
		cout << "upper bound = " << UB << '\t' << "z_star = " << z_star << '\t' << "lower bound = " << LB << endl;
//		cout << "upper bound = " << z_star << '\t' << "lower bound = " << LB << endl;

		// Check termination
		if( abs( z_star - LB ) / min( abs( LB ), abs( z_star ) ) < FLT_EPSILON )
		{
			puts( "gap minimum" );
			printf( "Stop at iteration: %d\n", iter );
			return;
		}
		else if( iter == ITER_LIMIT )
		{
			puts( "iteration limit reached" );
			return;
		}
		else if( LB >= z_star )
		{
			puts( "LB >= z_star" );
			printf( "Stop at iteration: %d", iter );
			return;
		}

		// Adjust multiplier
		if( NumofImprovement == IMPROVE_LIMIT )
			lambda /= 2, NumofImprovement = 0;
		adjust_multiplier();
	}
}

double sol_sub1()
{
	memset( a, 0, sizeof( a ) );
	for( int i = 0; i < TASK; ++i )
	{
		vector<tuple<double,int,int>> server_decision;
		int arrival = 0;

		// delta[ arrival ][ i + 1 ] 還是 0 的時候,表示它還沒有來,要移到下個時間
		while( delta[ arrival ][ i + 1 ] == 0 )
			++arrival;
//		if arrival + GAMMA > time...reject?continue

		for( int t = arrival; t < min( TIME, arrival + epsilon ); ++t )
		{
			vector<pair<double,int>> coef;
			for( int s = 0; s < SERVER; ++s )
				coef.push_back( make_pair( -value[ t ][ i + 1 ] * delta[ t ][ i + 1 ]
								+ mu1[ t ][ i ][ s ] * delta[ t ][ i + 1 ]
								+ mu2[ t ][ s ] * delta[ t ][ i + 1 ] * cpu[ t ][ i + 1 ]
								+ mu3[ t ][ s ] * delta[ t ][ i + 1 ] * memory[ t ][ i + 1 ]
								- mu5[ i ] * delta[ t ][ i + 1 ] / GAMMA[ i ]
								+ mu6[ i ] * delta[ t ][ i + 1 ], s ) );

			// Find min coefficient in S, after its arrival
			//see( coef );
			auto minimum = min_element( coef.begin(), coef.end() );
			//cout << minimum - coef.begin() << endl;
			if( minimum->first < 0 )
				server_decision.push_back( make_tuple( minimum->first, t, minimum->second ) );
		}

		sort( server_decision.begin(), server_decision.end() );
		for( int n = 0; n < GAMMA[ i ] && n < server_decision.size(); ++n )
			a[ get<1>( server_decision[ n ] ) ][ i ][ get<2>( server_decision[ n ] ) ] = 1;
	}

	return cal_sub1();
}

double sol_sub2()
{
	memset( x, 0, sizeof( x ) );
	for( int t = 0; t < TIME; ++t )
	{
		priority_queue<tuple<double,int,int>> records;
//		const int EST_SERVER = max( 1., max( ceil( double( opt_cpu[ t ] ) / BASE_CPU ), ceil( double( opt_mem[ t ] ) / BASE_MEMORY ) ) );

		for( int s = 0; s < SERVER; ++s )
			if( t < TIME - 1 )
			{
				double coef = FIX_COST[ s ]
							- mu2[ t ][ s ] * CPU[ s ]
							- mu3[ t ][ s ] * MEMORY[ s ]
							+ mu4[ t ][ s ]
							- mu4[ t + 1 ][ s ];
				for( int i = 0; i < TASK; ++i )
					coef -= mu1[ t ][ i ][ s ];

				if( coef < 0 )
					x[ t ][ s ] = 1;
				else
				{
					x[ t ][ s ] = 0;
					records.push( make_tuple( -coef, t, s ) );
//					printf( "insert (coef,t,s) = (%f,%d,%d)\n", -coef, t, s );
//					printf( "top (coef,t,s) = (%f,%d,%d)\n", get<0>( records.top() ), get<1>( records.top() ), get<2>( records.top() ) );
				}
			}
			else // t = TIME
			{
				double coef = FIX_COST[ s ]
							- mu2[ t ][ s ] * CPU[ s ]
							- mu3[ t ][ s ] * MEMORY[ s ]
							+ mu4[ t ][ s ];
				for( int i = 0; i < TASK; ++i )
					coef -= mu1[ t ][ i ][ s ]; // same above

				if( coef < 0 )
					x[ t ][ s ] = 1;
				else // t = TIME
				{
					x[ t ][ s ] = 0;
					records.push( make_tuple( -coef, t, s ) );
				}
			}

		int open = accumulate( x[ t ], x[ t + 1 ], 0 );

		// Make to satisfy constraint
		for( ; open < OPEN; ++open )
		{
			x[ get<1>( records.top() ) ][ get<2>( records.top() ) ] = 1;
			records.pop();
		}
	}

	return cal_sub2();
}

double sol_sub3()
{
	memset( y, 0, sizeof( y ) );

	// look front and back to set zero.

	double **coef = new double*[ TIME ];

	for( int i = 0 ; i < TIME; ++i )
		coef[ i ] = new double[ SERVER ];

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			coef[ t ][ s ] = REOPEN_COST[ s ] - mu4[ t ][ s ];
//				records.push( make_tuple( REOPEN_COST[ s ] - mu4[ t ][ s ], t, s ) );

	// Make to satisfy constraint
	for( int s = 0; s < SERVER; ++s )
	{
		double *val = new double[ TIME ], *sol = new double[ TIME ];
		bool *use = new bool[ TIME ];

		for( int t = 0; t < TIME; ++t )
		{
			val[ t ] = coef[ t ][ s ];
			use[ t ] = sol[ t ] = 0;
		}

		for( int t = 2; t < TIME; ++t )
			if( val[ t ] + sol[ t - 2 ] < sol[ t - 1 ] )
				sol[ t ] = val[ t ] + sol[ t - 2 ], use[ t ] = true;
			else
				sol[ t ] = sol[ t - 1 ];

		for( int t = TIME - 1; t >= 0; )
			if( use[ t ] )
				y[ t ][ s ] = 1, t -= 2;
            else
				--t;

		delete[] val, delete[] sol, delete[] use;

		// O(N^2) solution
		/*
		double *val = new double[ TIME ], **dp = new double*[ TIME ], opt_val = 0;
		bool **use = new bool*[ TIME ];
		int opt_take = 0;

		// Allocate memory
		for( int t = 0; t < TIME; ++t )
		{
			val[ t ] = coef[ t ][ s ];
			dp[ t ] = new double[ TIME ];
			use[ t ] = new bool[ TIME ];
		}

		// Initialize
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < TIME; ++s )
				dp[ t ][ s ] = s <= t / 2? 0 : INT_MAX, use[ t ][ s ] = false;

		// Real DP here
		if( val[ 1 ] < 0 )
			use[ 1 ][ 1 ] = true, dp[ 1 ][ 1 ] = val[ 1 ];
		for( int time = 2; time < TIME; ++time )
			for( int take = 1; take <= time / 2; ++take )
			{
				if( val[ time ] + dp[ time - 2 ][ take - 1 ] < dp[ time - 1 ][ take ] ) // take is good
				{
					use[ time ][ take ] = true;
					dp[ time ][ take ] = val[ time ] + dp[ time - 2 ][ take - 1 ];
				}
				else
				{
					use[ time ][ take ] = false;
					dp[ time ][ take ] = dp[ time - 1 ][ take ];
				}
			}

		// How many we should take
		for( int i = 1; i < TIME / 2; ++i )
			if( dp[ TIME - 1 ][ i ] < opt_val )
				opt_val = dp[ TIME - 1 ][ opt_take = i ];
		// Find which time to set
		for( int i = TIME - 1, j = opt_take; j; )
			if( use[ i ][ j ] )
			{
				i -= 2;
				y[ i ][ s ] = 1;
				--j;
			}
			else
				--i;

		// Delete memory
		for( int t = 0; t < TIME; ++t )
			delete[] dp[ t ], delete[] use[ t ];
		delete[] val;
		delete[] dp;
		delete[] use;
		*/
	}

	for( int i = 0; i < TIME; ++i )
		delete[] coef[ i ];
	delete[] coef;

	return cal_sub3();
}

double sol_sub4()
{
	const int ok_block = TASK * block_rate + DBL_EPSILON;
	vector<pair<double,int>> coef;

	memset( b, 0, sizeof( b ) );

	for( int i = 0; i < TASK; ++i )
		coef.push_back( make_pair( penalty[ i ] - mu5[ i ] + mu6[ i ], i ) ); // (coef,index)
	sort( coef.begin(), coef.end() ); // 係數已照大小排好 (ascending)

	//check??? i should be sum of bi
	for( int idx = 0; idx < ok_block && coef[ idx ].first < 0; ++idx )
		b[ coef[ idx ].second ] = 1;

	return cal_sub4();
}

double cal_sub1()
{
	double ans = 0;

    for( int i = 0; i < TASK; ++i )
	{
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				ans += ( -value[ t ][ i + 1 ]
							+ mu1[ t ][ i ][ s ]
							+ mu2[ t ][ s ] * cpu[ t ][ i + 1 ]
							+ mu3[ t ][ s ] * memory[ t ][ i + 1 ]
							- mu5[ i ] / GAMMA[ i ]
							+ mu6[ i ] ) * delta[ t ][ i + 1 ] * a[ t ][ i ][ s ];
		ans += mu5[ i ];
	}

	return ans;
}

double cal_sub2()
{
	double ans = 0;

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			if( t < TIME - 1 )
			{
				double coef = FIX_COST[ s ] - mu2[ t ][ s ] * CPU[ s ] - mu3[ t ][ s ] * MEMORY[ s ] + mu4[ t ][ s ] - mu4[ t + 1 ][ s ];
				for( int i = 0; i < TASK; ++i )
					coef -= mu1[ t ][ i ][ s ];
				ans += coef * x[ t ][ s ];
			}
			else
			{
				double coef = FIX_COST[ s ] - mu2[ t ][ s ] * CPU[ s ] - mu3[ t ][ s ] * MEMORY[ s ] + mu4[ t ][ s ];
				for( int i = 0; i < TASK; ++i )
					coef -= mu1[ t ][ i ][ s ];
				ans += coef * x[ t ][ s ];
			}

	return ans;
}

double cal_sub3()
{
	double ans = 0;

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			ans += ( REOPEN_COST[ s ] - mu4[ t ][ s ] ) * y[ t ][ s ];

	return ans;
}

double cal_sub4()
{
	double ans = 0;

	for( int i = 0; i < TASK; ++i )
		ans += ( penalty[ i ] - mu5[ i ] + mu6[ i ] ) * b[ i ];

	return ans;
}

double primal()
{
	// Copy for primal solution use
//	copy( a[ 0 ][ 0 ], a[ TIME ][ 0 ], pa[ 0 ][ 0 ] );
//	copy( x[ 0 ], x[ TIME ], px[ 0 ] );
//	copy( y[ 0 ], y[ TIME ], py[ 0 ] );
//	copy( b, b + TASK, pb );

	double primal_feasible_solution = 0;

/*
	// 同樣,這裡的 CPU 也要加上 [s], 不過還沒改, 將來有天要用的話還是要加上去

	// Heuristic--->Knapsack, check constraint 3.3 3.4

	vector<bool> served( TASK + 1, false );
	srand( SEED );
	//memset( pa, 0, sizeof( pa ) );
	for( int i = 1; i <= TASK; ++i )
	{
		value[ 0 ][ i ] = rand() % MAX_V + 1;
		cpu[ 0 ][ i ] = rand() % CPU + 1;
		memory[ 0 ][ i ] = rand() % MEMORY + 1;
	}

	for( int t = 0; t < TIME; ++t )
	{
		for( int s = 0; s < SERVER; ++s )
			if( px[ t ][ s ] == 1 )
			{
				memset( gain, 0, sizeof( gain ) );
				for( auto& a: content )
					for( auto& b: a )
						for( auto& c: b )
							c.clear();

				for( int i = 1; i <= TASK; ++i )
					for( int j = 1; j <= CPU; ++j )
						for( int k = 1; k <= MEMORY; ++k )
							//if( !served[ i ] && cpu[ t ][ i ] <= j && memory[ t ][ i ] <= k && better( i, j, k, t ) )
							if( delta [ t ] [ i + 1 ] == 1 && !served[ i ] && cpu[ t ][ i ] <= j && memory[ t ][ i ] <= k && better( i, j, k, t ) )
							{
//								if( t == 3 and s == 2 and i == 10 and j == 100 and k == 100 )
//									see( content[ 9 ][ 74 ][ 21 ] );
								gain[ i ][ j ][ k ] = value[ t ][ i ] + gain[ i - 1 ][ j - cpu[ t ][ i ] ][ k - memory[ t ][ i ] ];
								content[ i ][ j ][ k ] = content[ i - 1 ][ j - cpu[ t ][ i ] ][ k - memory[ t ][ i ] ];
								content[ i ][ j ][ k ].push_back( i );
							}
							else if( delta [ t ] [ i + 1 ] == 1 )

							{
								gain[ i ][ j ][ k ] = gain[ i - 1 ][ j ][ k ];
								content[ i ][ j ][ k ] = content[ i - 1 ][ j ][ k ];
							}

				for( auto item: content[ TASK ][ CPU ][ MEMORY ] )
				{
					served[ item ] = true;
					pa[ t ][ item - 1 ][ s ] = 1;
//					printf( "Time %d Server %d Item %d\n", t, s, item );
				}
			}
		for( int i = 1; i <= TASK; ++i )
			if( !served[ i ] )
			{
				cpu[ t + 1 ][ i ] = cpu[ t ][ i ];
				memory[ t + 1 ][ i ] = memory[ t ][ i ];
				value[ t + 1 ][ i ] = value[ t ][ i ];
			}
	}
*/

	// Heuristic--->用大小來丟東西,換新的作法來用,這個先不用了
	// px 跟著 pa 走,所以先改 pa,而不照限制式的順序

	vector<bool> put( TASK, false );

	for( int s = 0; s < SERVER && find( put.begin(), put.end(), false ) != put.end(); ++s )
	{
		for( int t = 0; t < TIME; ++t )
		{
			vector<int> pending;
			for( int i = 0; i < TASK; ++i )
				if( !put[ i ] && arrive[ i ] <= t )
					pending.push_back( i + 1 );
			sort( pending.begin(), pending.end(), [ t ] ( const int& a, const int& b ) { return cpu[ t ][ a ] > cpu[ t ][ b ]; } );
//			see( pending );
//			see( put );
			for( int c = 0, m = 0, i = 0; i < pending.size(); ++i )
				if( c + cpu[ t ][ pending[ i ] ] <= CPU[ s ] && m + memory[ t ][ pending[ i ] ] <= MEMORY[ s ] && !put[ pending[ i ] - 1 ] )
				{
//					printf( "Task %d insert to server %d at time %d\n", pending[ i ], s, t );
					put[ pending[ i ] - 1 ] = true;
					pa[ t ][ pending[ i ] - 1 ][ s ] = 1;
					c += cpu[ t ][ pending[ i ] ];
					m += memory[ t ][ pending[ i ] ];
				}
			cout << "";
		}
	}

	/* 原版的 primal 調法
	// check constraint (3.3) CPU capacity
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			double used_cpu = cal_primal( pcpu, t, s );

			// if server_s is off, RHS = 0, so LHS must be 0, too
			if( used_cpu > px[ t ][ s ] * CPU[ s ] )
			{
				// Heuristic CP ratio ----- Remove tasks until to demands lower than capacity
				int *id = new int[ TASK ];
				for( int i = 0; i < TASK; ++i )
					id[ i ] = i + 1;
				if( heu3 == CP_SORT )
					sort( id, id + TASK, [ t ] ( int a, int b ) { return double( value[ t ][ a ] ) / pcpu[ t ][ a ] < double( value[ t ][ b ] ) / pcpu[ t ][ b ]; } );
				else if( heu3 == MU_SORT )
					sort( id, id + TASK, [ t ] ( int a, int b ) { return pcpu[ a ] > pcpu[ b ]; } );

				for( int i = 0; used_cpu > px[ t ][ s ] * CPU[ s ]; ++i )
					if( pa[ t ][ id[ i ] - 1 ][ s ] )
					{
						used_cpu -= delta[ t ][ id[ i ] ] * pa[ t ][ id[ i ] - 1 ][ s ] * pcpu[ t ][ id[ i ] ];
						pa[ t ][ id[ i ] - 1 ][ s ] = 0;

						// Find a new server to store this task
						// 等到下個server/time再去煩惱吧,先往後丟就對了
						// Best fit, First fit.
						if( s < SERVER - 1 )
							pa[ t ][ id[ i ] - 1 ][ s + 1 ] = 1;
						else if( t < TIME - 1 ) // last server, move to next time,但到最後一個時間的時候就不丟了
							pa[ t + 1 ][ id[ i ] - 1 ][ 0 ] = 1;
					}
				delete[] id;
			}
		}


	// Heuristic II --> Mu Adjustment
	// find max mu, adj constraint
	// 3.3 -> find mu2
	// sort mu2, find max of mu2
	// 3.4 -> find mu3

	// check constraint (3.4) memory capacity
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			double used_memory = cal_primal( pmemory, t, s );

			// if server_s is off, RHS = 0, so LHS must be 0, too
			if( used_memory > px[ t ][ s ] * MEMORY[ s ] )
			{
//					printf( "constraint(3.4) not satisfied\n" );
				int *id = new int[ TASK ];
				for( int i = 0; i < TASK; ++i )
					id[ i ] = i + 1;
				if( heu4 == CP_SORT )
					sort( id, id + TASK, [ t ] ( int a, int b ) { return double( value[ t ][ a ] ) / pmemory[ t ][ a ] < double( value[ t ][ b ] ) / pmemory[ t ][ b ]; } );
				else if( heu4 == MU_SORT )
					sort( id, id + TASK, [ t ] ( int a, int b ) { return pmemory[ a ] > pmemory[ b ]; } );
				for( int i = 0; used_memory > px[ t ][ s ] * MEMORY[ s ]; ++i )
					if( pa[ t ][ id[ i ] - 1 ][ s ] )
					{
						used_memory -= delta[ t ][ id[ i ] ] * pa[ t ][ id[ i ] - 1 ][ s ] * pmemory[ t ][ id[ i ] ];
						pa[ t ][ id[ i ] - 1 ][ s ] = 0;

						// Find a new server to store this task
						if( s < SERVER - 1 )
							pa[ t ][ id [ i ] - 1 ][ s + 1 ] = 1;
						else if( t < TIME - 1 )
							pa[ t + 1 ][ id[ i ] - 1 ][ 0 ] = 1;
					}
				delete[] id;
			}
		}
*/



	// check constraint (3.2)
	for( int t = 0; t < TIME; ++t )
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				if( delta[ t ][ i + 1 ] * pa[ t ][ i ][ s ] > px[ t ][ s ] )
					px[ t ][ s ] = pa[ t ][ i ][ s ];


	// check constraint (3.6)

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			if( px[ t ][ s ] && ( !t || !px[ t - 1 ][ s ] ) && any_of( px, px + t, [ s ] ( double *n ) { return n[ s ] == 1; } ) )
				py[ t ][ s ] = 1;
			else
				py[ t ][ s ] = 0;

	// check constraint (3.5,3.6)

	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += delta[ t ][ i + 1 ] * pa[ t ][ i ][ s ];
		pb[ i ] = tmp != GAMMA[ i ]; // 不相等就設 1
	}

	// calculate UB
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			primal_feasible_solution += FIX_COST[ s ] * px[ t ][ s ] + REOPEN_COST[ s ] * py[ t ][ s ];
			for( int i = 0; i < TASK; ++i )
				primal_feasible_solution += -value[ t ][ i + 1 ] * delta[ t ][ i + 1 ] * pa[ t ][ i ][ s ];
		}
	for( int i = 0; i < TASK; ++i )
		primal_feasible_solution += penalty[ i ] * pb[ i ];

	return primal_feasible_solution;
}

double cal_primal( double use[ TIME ][ TASK + 1 ], int t, int s )
{
	double sum = 0;
	for( int i = 0; i < TASK; ++i )
		sum += delta[ t ][ i + 1 ] * pa[ t ][ i ][ s ] * use[ t ][ i + 1 ];
	return sum;
}

void adjust_multiplier()
{
	double denominator = 0, size;

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			double tmp = 0;

			for( int i = 0; i < TASK; ++i )
				denominator += pow( delta[ t ][ i + 1 ] * a[ t ][ i ][ s ] - x[ t ][ s ], 2 );

			for( int i = 0; i < TASK; ++i )
				tmp += a[ t ][ i ][ s ] * delta[ t ][ i + 1 ] * cpu[ t ][ i + 1 ];
			tmp -= x[ t ][ s ] * CPU[ s ];
			denominator += pow( tmp, 2 );

			tmp = 0;
			for( int i = 0; i < TASK; ++i )
				tmp += a[ t ][ i ][ s ] * delta[ t ][ i + 1 ] * memory[ t ][ i + 1 ];
			tmp -= x[ t ][ s ] * MEMORY[ s ];
			denominator += pow( tmp, 2 );

			denominator += pow( x[ t ][ s ] - ( t? x[ t - 1 ][ s ] : 0 ) - y[ t ][ s ], 2 );
		}
	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;

		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += delta[ t ][ i + 1 ] * a[ t ][ i ][ s ];
		denominator += pow( ( GAMMA[ i ] - tmp ) / GAMMA[ i ] - b[ i ], 2 );

		// tmp 沿用 (值相同)
		denominator += pow( b[ i ] - GAMMA[ i ] + tmp, 2 );
	}
//	size = lambda * ( z_star - LB ) / denominator; // LB may be zd
	size = lambda * ( z_star - zd ) / denominator; // LB may be zd


	// mu1
	for( int t = 0; t < TIME; ++t )
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				mu1[ t ][ i ][ s ] = max( 0., mu1[ t ][ i ][ s ] + size * ( delta[ t ][ i + 1 ] * a[ t ][ i ][ s ] - x[ t ][ s ] ) );

	// mu2
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			double tmp = 0;
			for( int i = 0; i < TASK; ++i )
				tmp += a[ t ][ i ][ s ] * delta[ t ][ i + 1 ] * cpu[ t ][ i + 1 ];
			mu2[ t ][ s ] = max( 0., mu2[ t ][ s ] + size * ( tmp - x[ t ][ s ] * CPU[ s ] ) );
		}

	// mu3
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			double tmp = 0;
			for( int i = 0; i < TASK; ++i )
				tmp += a[ t ][ i ][ s ] * delta[ t ][ i + 1 ] * memory[ t ][ i + 1 ];
			mu3[ t ][ s ] = max( 0., mu3[ t ][ s ] + size * ( tmp - x[ t ][ s ] * MEMORY[ s ] ) );
		}

	// mu4
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			mu4[ t ][ s ] = max( 0., mu4[ t ][ s ] + size * ( x[ t ][ s ] - ( t? x[ t - 1 ][ s ] : 0 ) - y[ t ][ s ] ) );

	// mu5
	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += delta[ t ][ i + 1 ] * a[ t ][ i ][ s ];
		mu5[ i ] = max( 0., mu5[ i ] + size * ( ( GAMMA[ i ] - tmp ) / GAMMA[ i ] - b[ i ] ) );
	}

	// mu6
	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += delta[ t ][ i + 1 ] * a[ t ][ i ][ s ];
		mu6[ i ] = max( 0., mu6[ i ] + size * ( b[ i ] - GAMMA[ i ] + tmp ) );
	}
}
