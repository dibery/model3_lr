#include<bits/stdc++.h>
using namespace std;
/********************************************************************/
//  given parameters:
/********************************************************************/
const int N = 50,  ITER_LIMIT = 10, IMPROVE_LIMIT = 10, YS = 1000;
const int TAU = 3, TASK = 3, SERVER = 3;
bool exist[ TAU ][ TASK ];
double fix_cost[ SERVER ];
//typedef vector<int> Path;

/********************************************************************/
// decision variables of Relaxed LR-Problem:
/********************************************************************/

bool assign[ TAU ][ TASK ][ SERVER ] = { { { false } } };
int turn_on[ TAU ][ SERVER ], status_change[ TAU ][ SERVER ];

//double cap[ N ][ N ], flow[ N ][ N ], weight[ N ][ N ], fix_cost[ N ], mul[ 4 ];
//Path path[ N ][ N ];

/********************************************************************/
//  parameters of Lagrange Relaxation Method:
/********************************************************************/

double mu1[ TAU ][ TASK ], mu2[ TAU ][ TASK ][ SERVER ], mu3[ TAU ][ SERVER ], mu4[ TAU ][ SERVER ], mu5[ TAU ][ TASK ], mu6[ TAU ][ SERVER ], mu7[ SERVER ];
double CPU[ SERVER ], freq[ SERVER ], reserve[ SERVER ], demand[ TASK ], ram_t[ TASK ], ram_s[ SERVER ];
double epsilon = 1e-4, Z_star = 2, ZD = INT_MIN,  LB = INT_MIN, UB, lambda = 2, scale;
double sub_val[ 3 ];
int no_improve;

//void mcmf( int, int, int );
//bool augment_path( int, int, int );

//void init_multiplier();
//void init_ask();
//void init_server();
//double init_primal();

double sub_problem1();
double sub_problem2();
double sub_problem3();
void adjust_to_feasible();
void adjust_multiplier();
//void getPrimalFeasibleSolution1();
//void update_multiplier(scalar, iteration);

void init();

int main()
{
init:
	init();

	// Iteration begins
	for( int iter = 1; iter <= ITER_LIMIT; ++iter )
	{
		UB = 0;
		printf( "Iter #%d\n", iter );

sub1:
		/** Sub-problem 1 **/
		sub_val[ 0 ] = sub_problem1();

sub2:
		/** Sub-problem 2 **/
		sub_val[ 1 ] = sub_problem2();

sub3:
		/** Sub-problem 3 **/
		sub_val[ 2 ] = sub_problem3();

		ZD = accumulate( sub_val, sub_val + 3, 0. );
		for( int s = 0; s < SERVER; ++s )
		{
			double tmp = 0;
			for( int tau = 0; tau < TAU; ++tau )
				tmp += status_change[ tau ][ s ];
			ZD += mu7[ s ] * ( tmp - TAU / 2 );
		}

		if( ZD > LB )
		{
			LB = ZD;
			no_improve = 0;
		}
		else
			if( ++no_improve == IMPROVE_LIMIT )
				lambda /= 2, no_improve = 0;

feasible:
			/** Check feasibility **/
			adjust_to_feasible();

ub:
			/** Primal objective value -- upper bound **/

			for( int tau = 0; tau < TAU; ++tau )
				for( int s = 0; s < SERVER; ++s )
				{
					for( int t = 0; t < TASK; ++t )
						UB += assign[ tau ][ t ][ s ] * demand[ t ] / ( CPU[ s ] * freq[ s ] * reserve[ s ] );
					UB += fix_cost[ s ] * status_change[ tau ][ s ];
				}

update:
			/** Update bounds **/

			Z_star = min( Z_star, UB );
			puts( "update bounds success" );

			//printf( "Iter #%-4d\tTAU= #%-4d\tLB = %f\tUB = %f\nZ_star = %f\nZD = %f\n", iter, tau, LB, UB, Z_star,ZD );

see:
			/** See solutions **/

			puts( "A" );
			for( int i = 0; i < TAU; ++i )
			{
				puts( "---" );
				for( int j = 0; j < TASK; ++j )
					for( int k = 0; k < SERVER; ++k )
						printf( "%d%c", assign[ i ][ j ][ k ], k == SERVER - 1? '\n' : ' ' );
			}
			puts( "X" );
			for( int i = 0; i < TAU; ++i )
				for( int j = 0; j < SERVER; ++j )
					printf( "%d%c", turn_on[ i ][ j ], j == SERVER - 1? '\n' : ' ' );
			puts( "Y" );
			for( int i = 0; i < TAU; ++i )
				for( int j = 0; j < SERVER; ++j )
					printf( "%d%c", status_change[ i ][ j ], j == SERVER - 1? '\n' : ' ' );
terminate:
			/** Check termination **/

			if( abs( Z_star - LB ) / min( abs( Z_star ), abs( LB ) ) < epsilon || iter == ITER_LIMIT || LB > Z_star )
				return 0;

adj_mu:
			/** Adjust multiplier **/
			adjust_multiplier();

	} // End of an iteration
}

void init()
{
	// delta init.
	srand( 0 );
	for( int i = 0; i < TAU; ++i )
		for( int j = 0; j < TASK; ++j )
			exist[ i ][ j ] = rand() % 2 != 0; //true or false

	// graph, task, server init.
	// init. server and task

	for( int i = 0; i < SERVER; ++i )
		CPU[ i ] = 8, freq[ i ] = 3, reserve[ i ] = .98, ram_s[ i ] = 64, fix_cost[ i ] = 1;
	for( int i = 0; i < TASK; ++i )
		demand[ i ] = rand() % 8 + 1, ram_t[ i ] = rand() % 16 + 1;
}

double sub_problem1()
{
	double sum = 0;
	for( int tau = 0; tau < TAU; ++tau )
	{
		bool assigned[ TASK ] = { false };
		for( int i = 0; i < SERVER; ++i )
			for( int j = 0; j < TASK; ++j )
				if( !assigned[ j ] )
				{
					double tmp = 0;

					for( int s = 0; s < SERVER; ++s )
						for( int t = 0; t < TASK; ++t )
						{
							tmp += assign[ tau ][ t ][ s ] * demand[ t ] / CPU[ s ] / freq[ s ] / reserve[ s ];
							if( exist[ tau ][ t ] )
								tmp += mu1[ tau ][ t ] * ( assign[ tau ][ t ][ s ] );
							tmp += mu2[ tau ][ t ][ s ] * assign[ tau ][ t ][ s ];
							tmp += mu3[ tau ][ s ] * assign[ tau ][ t ][ s ] * demand[ t ];
							tmp += mu4[ tau ][ s ] * assign[ tau ][ t ][ s ] * ram_t[ t ];
						}
					if( exist[ tau ][ j ] )
						assigned[ j ] = assign[ tau ][ j ][ i ] = tmp >= 0;
					//assign[ tau ][ i ][ j ] = tmp < 0;
				}

		// Objective value of sub-problem 1
		for( int i = 0; i < TASK; ++i )
			for( int j = 0; j < SERVER; ++j )
				sum += assign[ tau ][ i ][ j ] * ( demand[ i ] / CPU[ j ] / freq[ j ] / reserve[ j ] \
						+ mu2[ tau ][ i ][ j ] + mu3[ tau ][ j ] * demand[ i ] + mu4[ tau ][ j ] * ram_t[ i ] );
	}
	//printf( "Objective value sub-problem 1 = %f\n", sum );
	return sum;
}

double sub_problem2()
{
	double sum = 0;
	for( int tau = 0; tau < TAU; ++tau )
	{
		for( int i = 0; i < SERVER; ++i )
		{
			//turn_on[ tau ][ i ] = 1;
			turn_on[ tau ][ i ] = 0;
			for( int s = 0; s < SERVER; ++s )
			{
				for( int t = 0; t < TASK; ++t )
				{
					sum -= mu2[ tau ][ t ][ s ] * turn_on[ tau ][ s ];
					if( exist[ tau ][ t ] )
						sum -= mu5[ tau ][ t ] * turn_on[ tau ][ s ];
				}
				sum -= mu3[ tau ][ s ] * turn_on[ tau ][ s ] * CPU[ s ] * freq[ s ] * reserve[ s ];
				sum -= mu4[ tau ][ s ] * turn_on[ tau ][ s ] * ram_s[ s ] * reserve[ s ];
				sum -= mu6[ tau ][ s ] * ( tau? turn_on[ tau - 1 ][ s ] : 0 );
				sum += mu6[ tau ][ s ] * turn_on[ tau ][ s ];
			}
			//turn_on[ tau ][ i ] = sum < 0;
			turn_on[ tau ][ i ] = sum < 0;
		}
	}
	//printf( "Objective value sub-problem 2 = %f\n", sum );
	return sum;
}

double sub_problem3()
{
	double sum = 0;
	fill( status_change[ 0 ], status_change[ TAU ], 0 );

	for( int tau = 0; tau < TAU; ++tau )
	{
		for( int i = 0; i < SERVER; ++i )
			status_change[ tau ][ i ] = max( turn_on[ tau ][ i ] - ( tau? turn_on[ tau - 1 ][ i ] : 0 ), 0 );

		for( int i = 0; i < SERVER; ++i )
			sum += ( fix_cost[ i ] - mu6[ tau ][ i ] + mu7[ i ] ) * status_change[ tau ][ i ];
	}
	//printf( "Objective value sub-problem 3 = %f\n", sum );
	return sum;
}

void adjust_to_feasible()
{
	bool feasible = true;
	for( int tau = 0; tau < TAU; ++tau )
	{
		// Chris:  add constraint (3.2)
		int sigma_a[ TASK ] = { 0 };
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				sigma_a[ i ] += assign[ tau ][ i ][ s ];

		for( int i = 0; i < TASK; ++i )
			if ( sigma_a[ i ] < exist[ tau ][ i ] )
			{
				puts( "Constraint 3.2 not satisfied but corrected" );
				//puts( "Constraint 3.2 not satisfied" ), feasible = false;
				assign[ tau ][ i ][ rand() % SERVER ] = true;//Chris: may change to fit residual capacity of servers
			}

		// 3.2-a -- always true Chris: add for double check.
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				if( assign[ tau ][ i ][ s ] > turn_on[ tau ][ s ] )
				{
					//puts( "Constraint 3.2-a not satisfied" ), feasible = false;
					puts( "Constraint 3.2-a not satisfied but corrected" );
					turn_on[ tau ][ s ] = true;
				}

		// 3.7-a
		for( int s = 0; s < SERVER; ++s )
		{
			double temp = 0;
			for( int i = 0; i < TASK; ++i )
				temp += assign[ tau ][ i ][ s ] * demand[ i ];
			if( temp > turn_on[ tau ][ s ] * CPU[ s ] * freq[ s ] * reserve[ s ] )
				puts( "Constraint 3.7-a not satisfied" ), feasible = false;
		}

		// 3.8-a
		for( int s = 0; s < SERVER; ++s )
		{
			double temp = 0;
			for( int i = 0; i < TASK; ++i )
				temp += assign[ tau ][ i ][ s ] * ram_t[ i ];
			if( temp > turn_on[ tau ][ s ] * ram_s[ s ] * reserve[ s ] )
				puts( "Constraint 3.8-a not satisfied" ), feasible = false;
		}

		// 3.9 -- always true Chris: add for double check.
		int sigma_x = count( turn_on[ tau ], turn_on[ tau + 1 ], true );
		for( int i = 0; i < TASK; ++i )
			if( sigma_x < exist[ tau ][ i ] )
				puts( "Constraint 3.9 not satisfied" ), feasible = false;


		// 3.9-a -- always true Chris: add for double check.
		for( int s = 0; s < SERVER; ++s )
			if( ( turn_on[ tau ][ s ] - ( tau? turn_on[ tau - 1 ][ s ] : 0 ) ) > status_change[ tau ][ s ] )
			{
				//puts( "Constraint 3.9-a not satisfied" ), feasible = false;
				puts( "Constraint 3.9-a not satisfied but corrected" );
				status_change[ tau ][ s ] = true;
			}

		// 3.10 -- always true  Chris: add for double check.
		for( int s = 0; s < SERVER; ++s )
		{
			int sigma_y = 0;
			for ( int t = 0; t < TAU; ++t )
				sigma_y += status_change[ t ][ s ];
			if( sigma_y < 0 || sigma_y > YS )
				puts( "Constraint 3.10 not satisfied" ), feasible = false;
		}
	}
}

void adjust_multiplier()
{
	double up = lambda * ( Z_star - ZD );

	for( int tau = 0; tau < TAU; ++tau )
	{
		// mu1
		puts( "mu1" );
		for( int t = 0; t < TASK; ++t )
		{
			double ax_plus_b = 0;

			for( int s = 0; s < SERVER; ++s )
				ax_plus_b += assign[ tau ][ t ][ s ];
			--ax_plus_b;
			if( ax_plus_b == 0 )
				ax_plus_b = 0.1;

			double down = pow( ax_plus_b, 2 ), scale = up / down;
			mu1[ tau ][ t ] = max( 0., mu1[ tau ][ t ] + scale * ax_plus_b );
			printf( "%.2f%c", mu1[ tau ][ t ], t == TASK - 1? '\n' : ' ' );
		}

		// mu2
		puts( "mu2" );
		for( int s = 0; s < SERVER; ++s )
			for( int t = 0; t < TASK; ++t )
			{
				double ax_plus_b = assign[ tau ][ t ][ s ] - turn_on[ tau ][ s ],
							 down = pow( ax_plus_b, 2 ), scale = up / down;
				//cout << mu2[ tau ][ t ][ s ] + scale * ax_plus_b << '\t';
				mu2[ tau ][ t ][ s ] = max( 0., mu2[ tau ][ t ][ s ] + scale * ax_plus_b );
				printf( "%.2f%c", mu2[ tau ][ t ][ s ], t == TASK - 1? '\n' : ' ' );
			}

		// mu3
		puts( "mu3" );
		for( int s = 0; s < SERVER; ++s )
		{
			double ax_plus_b = 0, down, scale;

			for( int t = 0; t < TASK; ++t )
				ax_plus_b += assign[ tau ][ t ][ s ] * demand[ t ];
			ax_plus_b -= turn_on[ tau ][ s ] * CPU[ s ] * freq[ s ] * reserve[ s ];
			down = pow( ax_plus_b, 2 );
			scale = up / down;

			mu3[ tau ][ s ] = max( 0., mu3[ tau ][ s ] + scale * ax_plus_b );
			printf( "%.2f%c", mu3[ tau ][ s ], s == SERVER - 1? '\n' : ' ' );
		}

		// mu4
		puts( "mu4" );
		for( int s = 0; s < SERVER; ++s )
		{
			double ax_plus_b = 0, down, scale;

			for( int t = 0; t < TASK; ++t )
				ax_plus_b += assign[ tau ][ t ][ s ] * ram_t[ t ];
			ax_plus_b -= turn_on[ tau ][ s ] * ram_s[ s ] * reserve[ s ];
			down = pow( ax_plus_b, 2 );
			scale = up / down;

			mu4[ tau ][ s ] = max( 0., mu4[ tau ][ s ] + scale * ax_plus_b );
			printf( "%.2f%c", mu4[ tau ][ s ], s == SERVER - 1? '\n' : ' ' );
		}

		// mu5
		puts( "mu5" );
		for( int t = 0; t < TASK; ++t )
		{
			double ax_plus_b = 0;

			for( int s = 0; s < SERVER; ++s )
				ax_plus_b += -turn_on[ tau ][ s ];
			ax_plus_b += exist[ tau ][ t ];

			double down = pow( ax_plus_b, 2 ), scale = up / down;
			mu5[ tau ][ t ] = max( 0., mu5[ tau ][ t ] + scale * ax_plus_b );
			printf( "%.2f%c", mu5[ tau ][ t ], t == TASK - 1? '\n' : ' ' );
		}

		// mu6
		puts( "mu6" );
		for( int s = 0; s < SERVER; ++s )
		{
			double ax_plus_b = turn_on[ tau ][ s ] - ( tau? turn_on[ tau - 1 ][ s ] : 0 ) - status_change[ tau ][ s ],
						 down = pow( ax_plus_b, 2 ), scale = up / down;

			mu6[ tau ][ s ] = max( 0., mu6[ tau ][ s ] + scale * ax_plus_b );
			printf( "%.2f%c", mu6[ tau ][ s ], s == SERVER - 1? '\n' : ' ' );
		}
	}
}
